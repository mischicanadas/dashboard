<?php
//Obtener info de placas de Baja California
//curl -X POST 'http://www3.ebajacalifornia.gob.mx/Placas/presupuestoPlaca.jsp?PLACA=AP27043&TRAMITE=C&MENSAJERIA=N&tipoPersona=N&usoVehiculo=N'
//Obtener info de placas de EUA
//curl -X POST 'https://www.carfax.com/processQuickVin.cfx?licensePlateNumber=XGA623&licensePlateState=IN'

ini_set('max_execution_time', 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_POST['Accion']))
{
	$Resultado = "";
    
	$Accion = $_POST["Accion"];
	
    switch($Accion)
    {
        case "Get_Placas": $Resultado = Get_Placas();break;
        case "Notify_Me": $Resultado = Notify_Me();break;
    }
    
    echo $Resultado;
}

if(isset($_FILES['upload_image'])) 
{
    $Placa = "Plate not detected";
    $target_dir = "../img/uploads/";
    $filename = uniqid_base36()."_".str_replace(' ', '', $_FILES["upload_image"]["name"]);
    $target_file = $target_dir . basename($filename);
    $uploadOk = 0;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    if(move_uploaded_file($_FILES["upload_image"]["tmp_name"], $target_file))
    {
        //Crop Image
        //$Imagen = run("./crop_image $target_file");
        $Placa = detect($target_file);
        if (file_exists($target_file))
            unlink($target_file);
    }
    echo $Placa;    
}

function Get_Placas()
{
    $Filtro = $_POST["Filtro"];

    $Query = "SELECT * FROM placas";
    $Row = "";
    $i = 0;

    switch($Filtro)
    {
        case "Now": $Query = "SELECT * FROM placas WHERE DATE(Fecha) = DATE(NOW())";break;
        case "All": $Query = "SELECT * FROM placas";break;
        default: $Query = "SELECT * FROM placas";
    }

    $result = Ejecuta_Query($Query);
    while($Registro = $result->fetch_assoc())
    {
        $Row.="<tr>";
            $Row.="<td>".$Registro['iPlaca']."</td>";
            $Row.="<td>".$Registro['Placa']."</td>";
            $Row.="<td><a style='color:#4169E1' href='assets/img/procesadas/".$Registro['Imagen']."' target='_blank'>".$Registro['Imagen']."</td>";
            $Row.="<td>".$Registro['Fecha']."</td>";
            
            $Row.= "<td class='td-actions text-right'>";
            $Row.= "<button type='button' rel='tooltip' class='btn btn-success btn-simple' data-original-title='' title=''>";
            $Row.=     "<i class='material-icons'>edit</i>";
            $Row.= "<div class='ripple-container'></div></button>";
            $Row.= "<button type='button' rel='tooltip' class='btn btn-danger btn-simple' data-original-title='' title=''>";
            $Row.=     "<i class='material-icons'>close</i>";
            $Row.= "<div class='ripple-container'></div></button>";
            $Row.= "</td>";
        $Row.="</tr>";
        $i++;
    }

    return $Row."|".$i;
}

function Notify_Me()
{
    $Name = $_POST["Name"];
    $Email = $_POST["Email"];

    return "$Name with email $Email added to mail list!";
}

function detect($Image)
{
    $Placa = "Plate not detected";
    $Country = "";

    //Placa US
    $Placa_US = run("alpr -c us -n 1 $Image | sed -n 2p | awk '{print $2}' ");
    $Placa_US = array_filter($Placa_US);
    //$Placa = $Placa_US[0];
    if(empty($Placa_US[0]))
        $Placa_US[0] = "";
    
    //Placa EU
    $Placa_EU = run("alpr -c eu -n 1 $Image | sed -n 2p | awk '{print $2}' ");
    $Placa_EU = array_filter($Placa_EU);

    if(empty($Placa_EU[0]))
        $Placa_EU[0] = "";
    
    //Placa AU
    $Placa_AU = run("alpr -c au -n 1 $Image | sed -n 2p | awk '{print $2}' ");
    $Placa_AU = array_filter($Placa_AU);

    if(empty($Placa_AU[0]))
        $Placa_AU[0] = "";
   
    if(strlen($Placa_US[0]) >= strlen($Placa_EU[0]) && strlen($Placa_US[0]) >= strlen($Placa_AU[0]))
    {
        $Placa = $Placa_US[0];
        $Country = " - US";
    }
    if(strlen($Placa_EU[0]) > strlen($Placa_US[0]) && strlen($Placa_EU[0]) > strlen($Placa_AU[0]))
    {
        $Placa = $Placa_EU[0];
        $Country = " - MX";
    }
    if(strlen($Placa_AU[0]) > strlen($Placa_EU[0]) && strlen($Placa_AU[0]) > strlen($Placa_US[0]))
    {
        $Placa = $Placa_AU[0];
        $Country = " - MX";
    }
            
    if(strlen($Placa_US[0]) == strlen($Placa_EU[0]) && strlen($Placa_US[0]) == strlen($Placa_AU[0]))
    {
        if($Placa_US != "")
        {
            $Placa = $Placa_US[0];
            $Country = " - US 01";
        }
            
    }

    return $Placa.$Country;
}

function run($command)
{
	$output = array();
	exec($command,$output);
	return $output;
}

function uniqid_base36($more_entropy=false) 
{
    $s = uniqid('', $more_entropy);
    if (!$more_entropy)
        return base_convert($s, 16, 36);
        
    $hex = substr($s, 0, 13);
    $dec = $s[13] . substr($s, 15); // skip the dot
    return base_convert($hex, 16, 36) . base_convert($dec, 10, 36);
}

function Ejecuta_Query ($Query)
{
    include("ConnDB.php");
    
    //Abre una conexion a MySQL server
    $mysqli = new mysqli($ConnDB["Servidor"],$ConnDB["Usuario"],$ConnDB["Password"],$ConnDB["DB"]);

    //Arrojo cualquier error tipo connection error
    if ($mysqli->connect_error) {
         die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $result = $mysqli->query($Query);
    //Cierro la conexion 
    $mysqli->close();
    
    return $result;
}

?>